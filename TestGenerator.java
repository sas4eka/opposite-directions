package directions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestGenerator {
    static final int N = 1000;
    static final int CHANGE_LIMIT = 10;
    static final int PROFIT_LIMIT = 400;

    static final int TESTS = 10;
    static final long BASE_SEED = 322;

    public static void main(String[] args) {
        for (int test = 1; test <= TESTS; test++) {
            long seed = test * BASE_SEED;
            List<List<Integer>> input = generateInput(seed);
            System.out.println(input);
        }
    }

    static List<List<Integer>> generateInput(long seed) {
        Random rnd = new Random(seed);
        List<List<Integer>> input = new ArrayList<>(N);
        for (int i = 0; i < N; i++) {
            int changeA = rnd.nextInt(2 * CHANGE_LIMIT + 1) - CHANGE_LIMIT;
            int profitA = rnd.nextInt(2 * PROFIT_LIMIT + 1) - PROFIT_LIMIT;
            int changeB = rnd.nextInt(2 * CHANGE_LIMIT + 1) - CHANGE_LIMIT;
            int profitB = rnd.nextInt(2 * PROFIT_LIMIT + 1) - PROFIT_LIMIT;
            input.add(List.of(changeA, profitA, changeB, profitB));
        }
        return input;
    }
}
