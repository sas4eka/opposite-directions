package directions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OppositeDirections {
    static final boolean DEBUG = false;
    public static final long TIME_LIMIT = 4500;

    public List<Integer> chooseOpportunities(List<List<Integer>> input) {
        long startTime = System.currentTimeMillis();

        int n = input.size();
        Opportunity[] opportunities = new Opportunity[n];
        for (int i = 0; i < n; i++) {
            opportunities[i] = new Opportunity(i,
                    input.get(i).get(0),
                    input.get(i).get(1),
                    input.get(i).get(2),
                    input.get(i).get(3)
            );
        }

        List<Integer> bestAnswer = range(n);
        long bestProfit = calculateProfit(opportunities, bestAnswer);

        long finishTime = startTime + TIME_LIMIT;
        for (int attempt = 0; System.currentTimeMillis() < finishTime; attempt++) {
            List<Integer> answer = range(n);
            Collections.shuffle(answer);
            if (attempt % 4 == 0) {
                answer.sort(Comparator.comparingDouble(o -> Math.signum(opportunities[o - 1].changeA)));
            } else if (attempt % 4 == 1) {
                answer.sort(Comparator.comparingDouble(o -> -Math.signum(opportunities[o - 1].changeA)));
            } else if (attempt % 4 == 2) {
                answer.sort(Comparator.comparingDouble(o -> Math.signum(opportunities[o - 1].changeB)));
            } else {
                answer.sort(Comparator.comparingDouble(o -> -Math.signum(opportunities[o - 1].changeB)));
            }

            climb(opportunities, answer, finishTime);

            long currentProfit = calculateProfit(opportunities, answer);
            if (currentProfit > bestProfit) {
                if (DEBUG) System.out.printf("BEST %.6f on %d\n", currentProfit * 0.000001, attempt + 1);
                bestAnswer = answer;
                bestProfit = currentProfit;
            } else {
                if (DEBUG) System.out.printf("CURR %.6f on %d\n", currentProfit * 0.000001, attempt + 1);
            }
        }

        if (DEBUG) System.out.printf("RES %.6f\n", bestProfit * 0.000001);
        return bestAnswer;
    }

    private List<Integer> range(int n) {
        List<Integer> res = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            res.add(i);
        }
        return res;
    }

    private void climb(Opportunity[] opportunities, List<Integer> answer, long finishTime) {
        int n = opportunities.length;
        int[] preChangeA = new int[n + 1];
        int[] preChangeB = new int[n + 1];
        int[] postProfitA = new int[n + 1];
        int[] postProfitB = new int[n + 1];

        int bestProfit = calculateProfit(opportunities, answer);
        List<Integer> permutation = range(n);
        boolean keepRunning = true;
        while (keepRunning && System.currentTimeMillis() < finishTime) {
            keepRunning = false;

            Collections.shuffle(permutation);

            for (int opportunityIndex : permutation) {
                Opportunity opportunity = opportunities[opportunityIndex - 1];
                answer.remove((Integer) opportunityIndex);

                fillPrefixes(opportunities, answer, preChangeA, preChangeB, postProfitA, postProfitB);

                int bestBenefit = 0;
                int bestPosition = -1;

                for (int position = 0; position <= answer.size(); position++) {
                    int benefit = preChangeA[position] * opportunity.profitA
                            + preChangeB[position] * opportunity.profitB
                            + opportunity.changeA * postProfitA[position]
                            + opportunity.changeB * postProfitB[position];
                    if (benefit >= bestBenefit) {
                        bestBenefit = benefit;
                        bestPosition = position;
                    }
                }
                if (bestPosition != -1) {
                    answer.add(bestPosition, opportunityIndex);
                }
                int currProfit = calculateProfit(opportunities, answer);
                if (currProfit > bestProfit) {
                    bestProfit = currProfit;
                    keepRunning = true;
                }
            }
        }
    }

    private void fillPrefixes(Opportunity[] opportunities, List<Integer> answer, int[] preChangeA, int[] preChangeB, int[] postProfitA, int[] postProfitB) {
        int m = answer.size();

        preChangeA[0] = 0;
        preChangeB[0] = 0;
        for (int i = 0; i < m; i++) {
            Opportunity opp = opportunities[answer.get(i) - 1];
            preChangeA[i + 1] = preChangeA[i] + opp.changeA;
            preChangeB[i + 1] = preChangeB[i] + opp.changeB;
        }

        postProfitA[m] = 0;
        postProfitB[m] = 0;
        for (int i = m - 1; i >= 0; i--) {
            Opportunity opp = opportunities[answer.get(i) - 1];
            postProfitA[i] = postProfitA[i + 1] + opp.profitA;
            postProfitB[i] = postProfitB[i + 1] + opp.profitB;
        }
    }

    private int calculateProfit(Opportunity[] opportunities, List<Integer> answer) {
        int totalProfit = 0;
        int positionA = 0;
        int positionB = 0;
        for (int index : answer) {
            Opportunity opportunity = opportunities[index - 1];
            positionA += opportunity.changeA;
            positionB += opportunity.changeB;
            totalProfit += positionA * opportunity.profitA + positionB * opportunity.profitB;
        }
        return totalProfit;
    }

    static class Opportunity {
        int index;
        int changeA;
        int profitA;
        int changeB;
        int profitB;

        public Opportunity(int index, int changeA, int profitA, int changeB, int profitB) {
            this.index = index;
            this.changeA = changeA;
            this.profitA = profitA;
            this.changeB = changeB;
            this.profitB = profitB;
        }
    }
}
