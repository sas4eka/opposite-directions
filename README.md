# Opposite Directions

## Problem Overview

This is an optimization problem. Finding the best solution can be nearly impossible, so you will work on an approximation algorithm.
The details of the result evaluation can be found in the `Scoring` section.

## Problem Statement

You are given a list of trading opportunities. Choose a subset of the opportunities and order them in the most
profitable way. There are two dimensions: `A` and `B`. Each opportunity has four fields encoded as a list: `[ changeA,
profitA, changeB, profitB ]`. The opportunities you select are processed in your order to compute the final profit. This works as
follows:

- The values `A`, `B`, `profit` are initially equal to `0`.
- Opportunities are processed one by one, performing the following:
    - `A += changeA`
    - `B += changeB`
    - `profit += A * profitA + B * profitB`

Your goal is to maximize the final value of `profit`.

Your task is to implement the method
```
List<Integer> chooseOpportunities(List<List<Integer>> opportunities)
```
It should return a list of 1-based indices in `opportunities`.

## Limitations
```
1 <= opportunities.size() <= 1000
-10 <= opportunity.change <= 10
-400 <= opportunity.profit <= 400
```
You may take each opportunity at most once.

## Scoring

The score is calculated as `max(0, profit) / 1_000_000_000` after processing all the taken opportunities in selected order.

## Test structure and time limits

There are 10 tests. Your final score for this problem is the average of your scores on these 10 tests.

Each test contains exactly 1000 opportunities. These opportunities are generated randomly, with change and profit values
chosen uniformly from the possible values.

Time limit is 5 seconds per test.

## Examples

### Example 1

```
opportunities = [ [ -3, 10, -10, -5 ], [ 1, 3, 3, 7 ] ]

result = [ 2, 1 ]
```
Taking the opportunity with index `2` results in `A = 1`, `B = 3`, `profit = 24`. The next opportunity then changes
those values to `A = -2`, `B = -7`, `profit = 39`. The final `profit` is `39`.

### Example 2

```
opportunities = [ [ 10, 10, 10, 10 ], [ -10, -10, -10, -10 ], [10, 10, 10, 10], [10, 10, 10, 10] ]

result = [ 1, 3, 4 ]
```
After processing the opportunities `1`, `3` and `4`, the final `profit` is `1200`.

# Solution

## Code

In this repository you can find the code of a relatively short solution to the given problem. It reliably produces scores between `0.2429` and `0.243`, which land between 11th and 14th place in the final leaderboard.

## Ideas and observations

### Core idea
The core idea of the solution is to produce multiple valid answers, calculate the score for each of them, and return the best result among all the candidates.

### Hill climbing
Hill climbing is an iterative technique that starts from a valid answer and improves it bit by bit. At some point the answer can't be improved anymore and the process stops.

The algorithm can perform iterative improvements by applying some operation to the existing answer. One of the possible operations does the following actions:
1) Choose a random element and temporarily remove it from the answer.
2) Insert the element back into the best possible location.

The process stops when every element reaches its optimal position. The obtained answer may not be the best overall, but we can't improve it by performing more operations.

### Shortening the answer
Some opportunities in the answer only reduce the profit, no matter where you relocate them. It may be beneficial to remove these opportunities from the answer and return the list with less than 1000 elements.

### Scoring optimizations
In general, the score of an answer can be calculated in a straightforward way with `O(n)` complexity.
If you use the naive score calculation for the proposed hill climbing, then every element will require `O(n^2)` operations to find its best position.
As we need to evaluate multiple positions of the same element, we can do it more efficiently.

Let's say that we are trying to insert the opportunity `opp` into a particular position in the answer.
We can estimate how the total profit will change after the insertion:
- Every opportunity `pre` located before the position will contribute `pre.changeA * opp.profitA + pre.changeB * opp.profitB`
- Every opportunity `post` located after the position will contribute `opp.changeA * post.profitA + opp.changeB * post.profitB`

The potential profit changes can be calculated efficiently by precomputing prefix sums of `change` and postfix sums of `profit`.
With these optimizations, we can evaluate all the positions in `O(n)` overall, which is `O(1)` per position.

### Retrying
After proper optimizations, the hill climbing can reach a local maximum in less than a second.
To use the whole time and reduce the variability of the result, run hill climbing many times with random seeds.
The provided solution can perform 20-30 random attempts within 5 seconds.

### Four major directions
It looks like every decent answer clearly prioritizes one of the four major directions (positive A, negative A, positive B, negative B).
It's hard to say in advance which of the four directions will lead to the best final result.
Trying all four directions can increase the expected score.
One of the possible ways to prioritize a particular direction is to pre-sort the inputs before running hill climbing.

### Alternatives
Obviously, the shortlist of the tricks above doesn't cover all the possibilities. There are much more ideas to explore:
- Replacing hill climbing with other optimization techniques (e.g. simulated annealing).
- Trying different operations for iterative answer modifications (e.g. swapping two elements of the answer).
- Maintaining efficient data structures with fast per-element updates and quick score evaluation (e.g. treaps, Fenwick trees)
